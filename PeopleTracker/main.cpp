#include <iostream>
#include <opencv2/opencv.hpp>
#include "Person.hpp"

using namespace std;

// intenzite rozmazania
const int BLUR = 80;
// sensitivita thresholdu
const int SENSITIVITY = 10;
// vyska pasu, v ktorom identifikuje prechody ludi
const int COUNTER_AREA_HEIGHT = 160;

// pocty ludi , ktory presli z hora nadol a naopak
int countUp = 0;
int countDown = 0;
int m_count = 0;

// rozpoznane osoby
std::vector<Person> persons;

// fps videa
double fps;

//int to string helper
string intToString(int number) {
	std::stringstream ss;
	ss << number;
	return ss.str();
}

void searchForMovement(cv::Mat thresholdImage, cv::Mat &cameraFeed) {
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;

	// hranice pasu (top, bottom) na zaznamenavanie prechodov ludi
	int topRect = (cameraFeed.size().height / 2) - COUNTER_AREA_HEIGHT / 2;
	int botRect = (cameraFeed.size().height / 2) + COUNTER_AREA_HEIGHT / 2;

	// vykreslime zaznamenavaci pas
	cv::rectangle(cameraFeed, cv::Point(0, topRect), cv::Point(cameraFeed.size().width, botRect), cv::Scalar(30, 30, 30));

	// najdeme kontury
	cv::findContours(thresholdImage, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_NONE);

	//premazeme ludi, ku ktorym nepatri ziadna z kontur
	for (int j = 0; j < persons.size(); j++) {
		bool keep = false;
		for (int i = 0; i < contours.size(); i++) {
			cv::Rect rect = cv::boundingRect(contours.at(i));
			if (persons.at(j).isSame(rect)) {
				keep = true;
				break;
			}
		}
		// ak nenaslo prislusnu kontura, osobu zmazeme - vysla mimo zaber kamery, a odpocitame z aktualneho mnozstva ludi v zabere
		if (!keep) {
			persons.erase(persons.begin() + j);
			m_count--;
		}
	}
	if (!contours.empty()) {
		// prechadzame kontury a hladame, ci podobne kontury nesedia s ulozenymi oblastmi ludi
		for (int i = 0; i < contours.size(); i++) {
			cv::Rect rect = cv::boundingRect(contours.at(i));
			cv::Point center = cv::Point(rect.x + rect.width / 2, rect.y + rect.height / 2);
			// ingorujeme male rozmery kontur (vacsinou zle identifikovana cast tela - zanedbame)
			if (rect.width > 80 && rect.height > 200) {
				bool found = false;
				for (int j = 0; j < persons.size(); j++) {
					// ak najdeme osobu s konturou (polohy stredov su priblizne rovnake), aktualizujeme jej data, nastavime novu oblast kontury (rect) a breakneme cyklus
					if (persons.at(j).isSame(rect)) {
						persons.at(j).setSpeed(rect, fps);
						persons.at(j).setNewBoundaries(rect);
						found = true;
						cv::rectangle(cameraFeed, rect.tl(), rect.br(), cv::Scalar(255, 0, 255));
						cv::putText(cameraFeed, persons.at(j).getSpeedString(), center, cv::FONT_HERSHEY_SIMPLEX, 2.0, cv::Scalar(0, 255, 0));
						// ak osoba este nebola zaznamenana
						if (persons.at(j).isActive()) {
							// a nachadza sa v oblasti pasu na zaznamenanie pohybu, tak ho zapocitaj podla toho, ci ide zhora alebo zdola a deaktivuj, aby ho viac uz nepocital
							if (rect.tl().y < botRect && rect.tl().y > topRect) {
								countUp++;
								persons.at(j).setDisabled();
							}
							if (rect.br().y < botRect && rect.br().y > topRect) {
								countDown++;
								persons.at(j).setDisabled();
							}
						}
						break;
					}
				}
				// ak nenaslo, vyvorime novu osobu a vlozime do vectoru
				if (!found) {
					Person person = Person(rect);
					// zvysime aktualny pocet ludi v zabere
					m_count++;
					persons.push_back(person);
				}
			}
		}
	}

	// vypisy statistik do framu - momentalny pocet ludi v zabere, ludia prechadzajuci nahor, ludia prechadzajuci nadol
	cv::putText(cameraFeed, "Current:", cv::Point(20, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));
	cv::putText(cameraFeed, std::to_string(m_count), cv::Point(210, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));
	cv::putText(cameraFeed, "Up:", cv::Point(280, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));
	cv::putText(cameraFeed, std::to_string(countUp), cv::Point(360, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));
	cv::putText(cameraFeed, "Down:", cv::Point(420, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));
	cv::putText(cameraFeed, std::to_string(countDown), cv::Point(580, 100), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 0, 0));

}

int main(int argc, const char * argv[]) {

	// frame = vykreslovaci frame, frame2 na zistovanie kontur ludi
	// grayFrame, grayFrame2 - pomocne frame v monochromatickej skale
	// threshold frame na 
	cv::Mat frame, frame2, grayFrame, grayFrame2, diffFrame, thresholdFrame;
	cv::VideoCapture capture;

	// odkaz na video
	capture.open("../data/video.mp4");
	// fps videa
	fps = capture.get(CV_CAP_PROP_FPS);

	if (!capture.isOpened()) {
		cout << "Error in opening file.";
		return -1;
	}
	
	// nastav rozmery capture okna
	capture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, 360);

	while (capture.get(CV_CAP_PROP_POS_FRAMES)<capture.get(CV_CAP_PROP_FRAME_COUNT) - 1) {
		// nacitanie jednotlivych framov
		capture.read(frame);
		capture.read(frame2);

		// prevod na grayscale
		cv::cvtColor(frame, grayFrame, cv::COLOR_BGR2GRAY);
		cv::cvtColor(frame2, grayFrame2, cv::COLOR_BGR2GRAY);

		// najdeme rozdiel medzi dvoji framemami
		cv::absdiff(grayFrame, grayFrame2, diffFrame);
		// vykoname segmentaciu obrazu
		cv::threshold(diffFrame, thresholdFrame, SENSITIVITY, 255, cv::THRESH_BINARY);

		// rozmazanie obrazu a znovu segmenentovanie (kvoli lepsiemu vyplneniu kontur)
		cv::blur(thresholdFrame, thresholdFrame, cv::Size(BLUR, BLUR));
		cv::threshold(thresholdFrame, thresholdFrame, SENSITIVITY, 255, cv::THRESH_BINARY);

		// metoda identifikaciu pohybu osob, urcovanie rychlosti a pocitanie prechodov
		searchForMovement(thresholdFrame, frame);

		// zmena rozmerov obrazu
		resize(frame, frame, cv::Size(1024, 768), 0, 0, cv::INTER_CUBIC);
		//resize(thresholdFrame, thresholdFrame, cv::Size(1024, 768), 0, 0, cv::INTER_CUBIC);

		imshow("Frame", frame);
		//imshow("Threshold", thresholdFrame);

		if (cv::waitKey(10) == 27) {
			return 1;
		}
	}

	return 0;
}
