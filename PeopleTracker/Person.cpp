//
//  Person.cpp
//  FaceRecognition
//
//  Created by Daniel Križan on 29/01/2018.
//  Copyright © 2018 default. All rights reserved.
//

#include "Person.hpp"

// tolerancia zmeny polohy stredu oblasti kontury
const int XDIFF = 50;
const int YDIFF = 150;
// koeficient urcenia, kolko pixelov je jeden meter
const int PIXELS_METER = 200;

Person::Person(cv::Rect rect) {
    m_boundaries = rect;
}

bool Person::isSame(cv::Rect rect) {
	cv::Point newCenter = getRectCenter(rect);
	cv::Point oldCenter = getRectCenter(m_boundaries);
	return (abs(newCenter.x - oldCenter.x) < XDIFF && abs(newCenter.y - oldCenter.y) < YDIFF);
}

void Person::setSpeed(cv::Rect rect, double fps) {
    cv::Point newCenter = getRectCenter(rect);
    cv::Point oldCenter = getRectCenter(m_boundaries);
    int xdiff = newCenter.x - oldCenter.x;
    int ydiff = newCenter.y - oldCenter.y;
    double speed = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
    m_speed = (speed / PIXELS_METER) * 3.6 * fps;
}

void Person::setActive() {
    m_active = true;
}

void Person::setDisabled() {
    m_active = false;
}

cv::Rect Person::getBoundaries() {
    return m_boundaries;
}

bool Person::isActive() {
    return m_active;
}

void Person::setNewBoundaries(cv::Rect rect) {
    m_boundaries = rect;
}

double Person::getSpeed() {
    return m_speed;
}

std::string Person::getSpeedString() {
    return std::to_string((int)getSpeed()) + "km/h";
}

cv::Point Person::getRectCenter(cv::Rect rect) {
    return cv::Point((rect.x + rect.width) / 2, (rect.y + rect.height) / 2);
}
