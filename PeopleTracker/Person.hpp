//
//  Person.hpp
//  FaceRecognition
//
//  Created by Daniel Križan on 29/01/2018.
//  Copyright © 2018 default. All rights reserved.
//

#ifndef Person_hpp
#define Person_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>

class Person {
private:
    cv::Rect m_boundaries;
    double m_speed;
    bool m_active = true;
    
public:
    Person(cv::Rect rect);
	// metoda na urcenie, ci kontura nepatri cloveku (rozdiel medi framemami)
	bool isSame(cv::Rect rect);
    // nastavi rychlost
    void setSpeed(cv::Rect rect, double fps);
	// nastavi osobu na aktivnu
    void setActive();
	// deaktivuje osobu (zamedzuje viacnasobnemu zapocitaniu)
    void setDisabled();
	// aktualizuj oblast kontur
    void setNewBoundaries(cv::Rect rect);
    
    // vrati rychlost
    double getSpeed();
    // vrati oblast kontur
    cv::Rect getBoundaries();
	// je osoba aktivna ? (tj nebola zapocitana)
    bool isActive();
	// vrati rychlost ako retazec
    std::string getSpeedString();
	// pomocna metoda na ziskanie stredu obdlzniku
    static cv::Point getRectCenter(cv::Rect rect);
};

#endif /* Person_hpp */
